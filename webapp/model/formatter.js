sap.ui.define([], function () {
    "use strict";
    return {
        broto: function(count , price) {
			return (Number(count)*Number(price)).toFixed(2) ;
		},
        getItemStatus: function(status , index){
            if(status === '1' && index === '1'){//error
                return 'Error'
            } 
            else if(status === '2' && index === '2'){//warning
                return 'Warning'
            }
            else if(status === '3' && index === '3'){//success
                return 'Success'
            }
            else return 'None'
        },
        getIconItemStatus:function(status , index){
            if(status === '1' && index === '1'){//error
                return 'sap-icon://open-folder'
            } 
            else if(status === '2' && index === '2'){//warning
                return 'sap-icon://lateness'
            }
            else if(status === '3' && index === '3'){//success
                return 'sap-icon://accept'
            }
            else return ''
        },
        getTooltipStatus:function(status , index){
            if(status === '1' && index === '1'){//error
                return 'ישנה הזמנה פתוחה שטרם הועברה לספק'
            } 
            else if(status === '2' && index === '2'){//warning
                return 'ישנה הזמנה אשר נשלחה לספק וטרם נקלטה למלאי'
            }
            else if(status === '3' && index === '3'){//success
                return 'אין הזמנות פתוחות '
            }
            else return ''
        }
    };
});
