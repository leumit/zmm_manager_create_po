sap.ui.define(["sap/ui/model/json/JSONModel", "sap/ui/Device","sap/ui/model/Filter"], function (JSONModel, Device,Filter) {
    "use strict";

    return {
        createDeviceModel: function () {
            var oModel = new JSONModel(Device);
            oModel.setDefaultBindingMode("OneWay");
            return oModel;
        },
        createJSONModel: function () {	
			var oModel = new sap.ui.model.json.JSONModel({
                 countSelectedRows: 0,
				 TotalVAT: 0 ,
				 TotalNetCost: 0 ,
				 TotalGrossCost: 0,
				 ReviewClicked:false,
				 orderType: '01',
				 ItemsClicked: false,
				 PriceOutput: false,
				 selectedBtn: true

			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
        LoadItems: function(sOrderNum , sVendor , sLogisticCenter){
            var aFilters = [];
			var oModel = oComponent_ManagerCreate.getModel("JSON").getData();
			if(sVendor !== '1000'){
				aFilters.push(new Filter("IvVendor", sap.ui.model.FilterOperator.EQ, sVendor));
			}
			if(!!sOrderNum){
				aFilters.push(new Filter("IvOrderNum", sap.ui.model.FilterOperator.EQ, sOrderNum));
			}
			if(sVendor === '1000'){
				aFilters.push(new Filter("IvLogisticCenter", sap.ui.model.FilterOperator.EQ, true));
			}
			aFilters.push(new Filter("IvPurorg", sap.ui.model.FilterOperator.EQ, oModel.EkorgSearch || ''));
			
            return new Promise(function(resolve, reject) {
				oComponent_ManagerCreate.getModel("ODATA").read("/GetItemDataSet", {
					filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
		
		LoadOutput: function(sOrderNum){
			var sKey = oComponent_ManagerCreate.getModel("ODATA").createKey("/OrderPdfOutputSet", {
				IvPoNumber : sOrderNum
			})
            return new Promise(function(resolve, reject) {
				oComponent_ManagerCreate.getModel("ODATA").read(sKey, {
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
		LoadMainAdrrSh: function(skostl){
            var aFilters = [];
            aFilters = [ new Filter("Kostl", sap.ui.model.FilterOperator.EQ, skostl)];
            return new Promise(function(resolve, reject) {
				oComponent_ManagerCreate.getModel("ODATA").read("/MainAddrShSet", {
					filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
		LoadSecAddrSh: function(sMainAdrnr){
            var aFilters = [];
            aFilters = [ new Filter("MainAdrnr", sap.ui.model.FilterOperator.EQ, sMainAdrnr)];
            return new Promise(function(resolve, reject) {
				oComponent_ManagerCreate.getModel("ODATA").read("/SecAddrShSet", {
					filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
        createOrder: function(oEntry){
            return new Promise(function(resolve, reject) {
				oComponent_ManagerCreate.getModel("ODATA").create("/OrderHeaderSet", oEntry,{
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	

        }

    };
});
