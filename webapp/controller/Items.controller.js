/* global oComponent_ManagerCreate  : true */
sap.ui.define(["ZMM_MANAGER_CREATE_PO/controller/BaseController",
    "ZMM_MANAGER_CREATE_PO/model/models", "sap/ui/model/Filter","sap/ui/model/FilterOperator", "sap/m/MessageBox", 'sap/m/MessageToast', "ZMM_MANAGER_CREATE_PO/model/formatter"], function (Controller, models, Filter, FilterOperator, MessageBox, MessageToast, formatter) {
        "use strict";

        return Controller.extend("ZMM_MANAGER_CREATE_PO.controller.Items", {
            formatter: formatter,

            onInit: function () {
                oComponent_ManagerCreate._ItemsController = this;
                this.oRouter = this.getOwnerComponent().getRouter();
                this.oRouter.getRoute('Items').attachPatternMatched(this.onItemsPress, this);
            },

            onItemsPress: function (oEvent) {
                var page = oComponent_ManagerCreate.getModel("JSON").getProperty("/page");
                oComponent_ManagerCreate.getModel("JSON").setProperty("/page", 'Items');
                var sOrderNum = oEvent.getParameter("arguments").OrderNumber;
                var sVendor = oComponent_ManagerCreate.getModel("JSON").getProperty("/Vendor") || '';
                oComponent_ManagerCreate.getModel("JSON").setProperty("/OrderNumber", sOrderNum || '');
                var order = sOrderNum || oComponent_ManagerCreate.getModel("JSON").getProperty("/OrderNumberCopy") || '';
                if (!oComponent_ManagerCreate.getModel("JSON").getProperty("/itemsTable")) {
                    this.getItems(order, sVendor, '', page);
                }

            },
            getItems: function (sOrderNum, sVendor, sLogisticCenter, prvPage) {
                var oModel = oComponent_ManagerCreate.getModel("JSON").getData();
                models.LoadItems(sOrderNum, sVendor, sLogisticCenter).then(function (data) {
                    if (data.results.length === 0 || (!sOrderNum && !prvPage)) {
                        MessageToast.show(oComponent_ManagerCreate.i18n('noRowsMsg'), {
                            duration: 1500
                        });
                        setTimeout(function () { oComponent_ManagerCreate._ItemsController.navTo('', 'Main'); return; }, 1000);

                    }
                    else if (!sOrderNum) {
                        for (var i = 0; i < data.results.length; i++) {
                            data.results[i].SecondaryAddress = oModel.secAddrKey;
                            data.results[i].SecondaryAddressName = oModel.secAddrInput;
                            data.results[i].PrimeryAddressTxt = oModel.MainAddrInput;
                            data.results[i].UnitPriceFloat = parseFloat(data.results[i].UnitPrice);
                            data.results[i].OrderQun = Number(data.results[i].OrderQun) === 0 ? '' : data.results[i].OrderQun;


                        }
                        oComponent_ManagerCreate.getModel("JSON").setProperty("/itemsTable", data.results);


                    }
                    else if (!!sOrderNum) {
                        var adrr = '';
                        if (!prvPage) {
                            models.LoadSecAddrSh(data.results[0].PrimeryAddress).then(function (oData) {
                                if (oData.results.length === 1) {
                                    adrr = oData.results[0].Adrnr;
                                }
                                oComponent_ManagerCreate.getModel("JSON").setProperty("/SecAddrSh", oData.results);
                                for (var i = 0; i < data.results.length; i++) {
                                    data.results[i].SecondaryAddress = oModel.secAddrKey || data.results[i].SecondaryAddress || adrr || data.results[i].PrimeryAddress;
                                    data.results[i].PrimeryAddressTxt = oModel.MainAddrInput || data.results[i].PrimeryAddressTxt;
                                    data.results[i].UnitPriceFloat = parseFloat(data.results[i].UnitPrice);
                                    data.results[i].OrderQun = Number(data.results[i].OrderQun) === 0 ? '' : data.results[i].OrderQun;
                                }
                                oComponent_ManagerCreate.getModel("JSON").setProperty("/itemsTable", data.results);
                            }).catch(function (error) {
                                console.log(error)
                            });

                        }
                        else {
                            for (var i = 0; i < data.results.length; i++) {
                                data.results[i].SecondaryAddress = oModel.secAddrKey || data.results[i].SecondaryAddress || adrr;
                                data.results[i].PrimeryAddressTxt = oModel.MainAddrInput || data.results[i].PrimeryAddressTxt;
                                data.results[i].UnitPriceFloat = parseFloat(data.results[i].UnitPrice);
                                data.results[i].OrderQun = Number(data.results[i].OrderQun) === 0 ? '' : data.results[i].OrderQun;
                            }
                            oComponent_ManagerCreate.getModel("JSON").setProperty("/itemsTable", data.results);

                        }

                        oComponent_ManagerCreate._ItemsController.onTableRowSelected('', data.results);
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            },
            beforeReview: function (sType) {
                var aItems = [];
                var rows = oComponent_ManagerCreate.getModel("JSON").getProperty("/itemsTable");
                if (sType === '2' && !!oComponent_ManagerCreate.getModel("JSON").getProperty("/OrderNumber")) {
                    sType === '4';
                }
                oComponent_ManagerCreate.getModel("JSON").setProperty("/ReviewClicked", true);
                for (var i = 0; i < rows.length; i++) {
                    if (!!rows[i].Selected) {
                        if (Number(rows[i].OrderQun) === 0 && sType !== '2') {
                            MessageToast.show(oComponent_ManagerCreate.i18n('noCountMsg'), {
                                duration: 1500
                            });
                            return;
                        }
                        else {
                            aItems.push(rows[i]);
                        }

                    }
                }
                if (aItems.length < 1) {
                    MessageToast.show(oComponent_ManagerCreate.i18n('noSelectedItemsMsg'), {
                        duration: 1500
                    });
                }
                else {
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/approvalItems", aItems);
                    this.onCreateOrder(sType, aItems);
                }

            },
            onPressComment: function (oEvent) {
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject();
                oComponent_ManagerCreate.getModel("JSON").setProperty("/oSelectedRow", oSelectedItem);
                oComponent_ManagerCreate.getModel("JSON").setProperty("/newItemText", oSelectedItem.ItemText);
                this.onOpenDialog('', 'comment');
            },
            saveCommentRow: function (oEvent) {
                var oModel = oComponent_ManagerCreate.getModel("JSON"),
                    oRow = oModel.getProperty("/oSelectedRow");
                oRow.ItemText = oModel.getProperty("/newItemText");
                oComponent_ManagerCreate.getModel("JSON").refresh();
                this.onCloseDialog('comment');
            },
            onDeleteOrder: function () {
                var aItems = [];
                this.onCreateOrder('5', aItems);
            },
            onlySelected: function (oEvent, flag) {
                var oFilters = [];
                oComponent_ManagerCreate.getModel("JSON").setProperty("/selectedBtn", !flag);
                if (flag) {
                    oFilters.push(new sap.ui.model.Filter("Selected", "EQ", true));
                }
                oComponent_ManagerCreate._ItemsController.byId("dataTable").getBinding("rows").filter(oFilters, "Application");

            },
            MicroProcessItemPress: function (oEvent, sType, row) {
                oComponent_ManagerCreate.getModel("JSON").setProperty("/microMsgType", sType);
                if (row !== sType) {
                    return;
                }
                if (!this._MicroProcessItem) {
                    this._MicroProcessItem = sap.ui.xmlfragment("ZMM_MANAGER_CREATE_PO.view.fragments.microProcessPopover", this);
                    this.getView().addDependent(this._MicroProcessItem);
                }
                this._MicroProcessItem.openBy(oEvent.getSource());
            },
            MicroProcessItemClose: function (oEvent) {
                this._MicroProcessItem.close();
            },
            enterOrderCount: function (oEvent) {
                debugger;
                var allItems = oComponent_ManagerCreate.getModel("JSON").getProperty("/itemsTable"),
                    TotalGrossCost = 0,
                    oRow = oEvent.getSource().getBindingContext("JSON").getObject(),
                    TaxValue = Number(oRow.TaxValue) / 100;
                oRow.OrderQun = Number(oRow.OrderQun) === 0 ? '' : parseInt(oRow.OrderQun);
                oComponent_ManagerCreate.getModel("JSON").refresh();
                for (var i = 0; i < allItems.length; i++) {
                    if (allItems[i].Selected) {
                        TotalGrossCost += parseInt(Number(allItems[i].OrderQun)) * Number(allItems[i].UnitPrice);
                    }
                }
                oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalGrossCost", TotalGrossCost.toFixed(2));
                oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalVAT", (TotalGrossCost * TaxValue).toFixed(2));
                oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalNetCost", (TotalGrossCost + TotalGrossCost * TaxValue).toFixed(2));
            },
            MatnrDescSearch: function (oEvent) {
                debugger;
                var aFilters = [];
                var sQuery = oEvent.getSource().getValue();
                if (sQuery && sQuery.length > 0) {
                    var filter = new Filter("MatnrTxt", FilterOperator.Contains, sQuery);
                    aFilters.push(filter);
                }

                // update table binding
                var oTable = this.byId("dataTable");
                var oBinding = oTable.getBinding("rows");
                oBinding.filter(aFilters, "Application");
            },
            onTableRowSelected: function (oEvent, items) {

                try {
                    oEvent.getSource().getBindingContext("JSON").getObject().Selected = oEvent.getParameter("selected");
                } catch { }
                var allItems = items || oComponent_ManagerCreate.getModel("JSON").getProperty("/itemsTable"),
                    TotalGrossCost = 0,
                    TotalVAT = 0,
                    countSelectedRows = 0,
                    TaxValue = Number(allItems[0].TaxValue) / 100;
                for (var i = 0; i < allItems.length; i++) {
                    if (allItems[i].Selected) {
                        TotalGrossCost += parseInt(Number(allItems[i].OrderQun)) * Number(allItems[i].UnitPrice);
                        countSelectedRows++;
                    }
                }
                oComponent_ManagerCreate.getModel("JSON").setProperty("/countSelectedRows", countSelectedRows);
                oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalGrossCost", TotalGrossCost.toFixed(2));
                oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalVAT", (TotalGrossCost * TaxValue).toFixed(2));
                oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalNetCost", (TotalGrossCost + TotalGrossCost * TaxValue).toFixed(2));

            }

        });
    });
