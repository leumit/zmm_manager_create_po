/* global oComponent_ManagerCreate  : true */
sap.ui.define(["ZMM_MANAGER_CREATE_PO/controller/BaseController",'sap/m/MessageToast',"sap/ui/model/Filter","ZMM_MANAGER_CREATE_PO/model/models"], function (Controller,MessageToast,Filter,models) {
    "use strict";

    return Controller.extend("ZMM_MANAGER_CREATE_PO.controller.Main", {
        onInit: function () {
            oComponent_ManagerCreate._MainController = this;
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oRouter.getRoute('Main').attachPatternMatched(this.onMain,this);
            this.oRouter.getRoute('Default').attachPatternMatched(this.onMain,this);
            
		},
        onMain: function(oEvent){
            oComponent_ManagerCreate.getModel("JSON").setProperty("/page" , 'Main');
            oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalVAT" , 0);
            oComponent_ManagerCreate.getModel("JSON").setProperty("/countSelectedRows" , 0);
            oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalNetCost" , 0);
            oComponent_ManagerCreate.getModel("JSON").setProperty("/TotalGrossCost" , 0);        
            oComponent_ManagerCreate.getModel("JSON").setProperty("/itemsTable" , '');   
            this.getDefaultValues();
            
        },
        getDefaultValues: function(oEvent){
            try{
                var PurchasingOrganization = oComponent_ManagerCreate.getComponentData().startupParameters["PurchasingOrganization"][0];
                oComponent_ManagerCreate.getModel("JSON").setProperty("/EkorgSearch" , PurchasingOrganization);
            }catch{}
            try{
                var PurchasingGroup = oComponent_ManagerCreate.getComponentData().startupParameters["PurchasingGroup"][0];
                oComponent_ManagerCreate.getModel("JSON").setProperty("/Ekgrp" , PurchasingGroup);
            }catch{}
            try{
                var Supplier = oComponent_ManagerCreate.getComponentData().startupParameters["Supplier"][0];
                oComponent_ManagerCreate.getModel("JSON").setProperty("/VendorSearch" , Supplier);
                if(Supplier === '1000'){
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/orderType" , '02');
                }
            }catch{}
            try{
                var CostCenter = oComponent_ManagerCreate.getComponentData().startupParameters["CostCenter"][0];
                // var CostCenter = "474545";
                oComponent_ManagerCreate.getModel("JSON").setProperty("/kostlKey" , CostCenter);
                oComponent_ManagerCreate.getModel("JSON").setProperty("/kostlInput" , CostCenter);    
                
                models.LoadMainAdrrSh(CostCenter).then(function(data) {
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/MainAdrrSh" , data.results);
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/kostlInput" , CostCenter);  
                    if(data.results.length === 1){
                        oComponent_ManagerCreate.getModel("JSON").setProperty("/mainAdrrKey" , data.results[0].Adrnr);
                        oComponent_ManagerCreate.getModel("JSON").setProperty("/MainAddrInput" , data.results[0].Adname);
                        models.LoadSecAddrSh(data.results[0].Adrnr).then(function(data) {
                            oComponent_ManagerCreate.getModel("JSON").setProperty("/SecAddrSh" , data.results);
                            if(data.results.length === 1){
                                oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrKey" , data.results[0].Adrnr);
                                oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrInput" , data.results[0].Adname); 
                                          
                            }
                        }).catch(function (error) {
                            console.log(error)
                        });
                         
                    }
                }).catch(function (error) {
                    console.log(error)
                });
            }catch{}
        },
        onChangeVendor: function(oEvent){
            var sVendorName = oEvent.getParameter("selectedItem").getProperty("text");
            oComponent_ManagerCreate.getModel("JSON").setProperty("/SelectedVendorName" , sVendorName);
        },
        
        goToItemsPage: function(oEvent){
            oComponent_ManagerCreate.getModel("JSON").setProperty("/ItemsClicked" , true);
            var oModel =oComponent_ManagerCreate.getModel("JSON").getData();
            var IvVendor = '';
            switch (oModel.orderType){
				case '01':{
					if(!oModel.VendorSearch || !oModel.kostlKey || !oModel.mainAdrrKey || !oModel.secAddrKey|| !oModel.EkorgSearch || !oModel.Ekgrp){
                        MessageToast.show(oComponent_ManagerCreate.i18n('MainErrorMsg'),{
                            duration: 1500});
                            return;
                    }
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/VendorName" , oModel.SelectedVendorName);
                    IvVendor = oModel.VendorSearch;
					break;
				}
				case '02':{
					if(!oModel.kostlKey || !oModel.mainAdrrKey || !oModel.secAddrKey|| !oModel.EkorgSearch || !oModel.Ekgrp){
                        MessageToast.show(oComponent_ManagerCreate.i18n('MainErrorMsg'),{
                            duration: 1500});
                            return;
                    }
                    else{
                        IvVendor = '1000';
                        oComponent_ManagerCreate.getModel("JSON").setProperty("/VendorName" , 'שיג');
                    }
                   
					break;
				}
                case '03':{				
                    if(!oModel.OrderNumberCopy || !oModel.kostlKey || !oModel.mainAdrrKey || !oModel.secAddrKey|| !oModel.EkorgSearch || !oModel.Ekgrp){
                        MessageToast.show(oComponent_ManagerCreate.i18n('MainErrorMsg'),{
                            duration: 1500});
                            return;
                    }
                    else{
                        IvVendor = oModel.VendorFromOrder;
                    }
					break;
				}
            
        }
        oComponent_ManagerCreate.getModel("JSON").setProperty("/Vendor" , IvVendor);
        sap.ui.core.UIComponent.getRouterFor(this).navTo("Items");
    }
    });
});
