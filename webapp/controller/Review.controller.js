/* global oComponent_ManagerCreate  : true */
sap.ui.define(["ZMM_MANAGER_CREATE_PO/controller/BaseController"], function (Controller) {
    "use strict";

    return Controller.extend("ZMM_MANAGER_CREATE_PO.controller.Review", {
        onInit: function () {
            oComponent_ManagerCreate._ReviewController = this;
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oRouter.getRoute('Review').attachPatternMatched(this.onReview,this);
		},
        onReview: function(){
            oComponent_ManagerCreate.getModel("JSON").setProperty("/page" , 'Review');
            var items =  oComponent_ManagerCreate.getModel("JSON").getProperty("/itemsTable") || '';
            if(!items){
                setTimeout(function () { oComponent_ManagerCreate._ReviewController.navTo('', 'Main'); return; }, 1000);
            }
        }
    });
});
