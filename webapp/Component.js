/* global oComponent_ManagerCreate  : true */
var oComponent_ManagerCreate;
sap.ui.define(
    ["sap/ui/core/UIComponent", "sap/ui/Device", "ZMM_MANAGER_CREATE_PO/model/models"],
    function (UIComponent, Device, models) {
        "use strict";

        return UIComponent.extend("ZMM_MANAGER_CREATE_PO.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                oComponent_ManagerCreate = this;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // enable routing
                this.getRouter().initialize();
                sap.ui.getCore().getConfiguration().setRTL(true);
                //Set HE language
                sap.ui.getCore().getConfiguration().setLanguage("iw_IL");
                //create json model
			    this.setModel(models.createJSONModel(), "JSON");
                // set the device model
                this.setModel(models.createDeviceModel(), "device");
                this.getModel("ODATA").attachBatchRequestSent(function(){
                    sap.ui.core.BusyIndicator.show();
                }).attachBatchRequestCompleted(function(event){
                    sap.ui.core.BusyIndicator.hide();
                });
            },
            i18n: function (str) {
                return oComponent_ManagerCreate.getModel("i18n").getProperty(str);
            }
        });
    }
);
